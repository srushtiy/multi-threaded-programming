# Multi-threaded programming

This program emulates/simulates a traffic shaper who transmits packets controlled by a
token bucket filter depicted below using multi-threading within a single process.
The program is implemented using pthread library in C.