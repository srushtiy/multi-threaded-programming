#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

#include <unistd.h>
#include <ctype.h>


#define BUF_SIZE 1026

#include "cs402.h"
#include "my402list.h"

int B,p;
int num;
double lambda,mu,r;
char *tsfile;
pthread_t packetArr,tokenDep,server1,server2,sigthread;
pthread_mutex_t m1=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
struct timeval timeStart, timeEnd;
My402List *Q1,*Q2,*packetList;
sigset_t set;
int packetDrop,packetsArr,packetsQ2;

typedef struct Bucket{
	int tokenCount;
	int B;
	double rate;
	int tokenAccepted;
	int tokenDropped;
} Bucket;

typedef struct Packet{
	int id;
	int tokenCount;
	double lambdaRate;
	double serviceTime;
	struct timeval q2enter;
	struct timeval q1enter;
	struct timeval arrivalTime;
} Packet;

typedef struct Stats{
	double p_interarrival_sum; // all packets 
	double serviceSum;
	double avg_q1;
	double avg_q2;
	double avg_s1;
	double avg_s2;
	double totalTime;
	double totalTimeSq;
} Stats;

void calcStats();
void printEmulationParameters();
void *print_message(void *ptr);
void *startToken(void *bucket);
void *generatePacket();
double timeDiff(struct timeval *start,struct timeval *end);
void *startServer1();
void *startServer2();
void *monitor(void *inp);
Bucket *myBucket;
Stats *myStats;
int mode,endVar;
FILE *fp;
char buffer[BUF_SIZE];
Packet *myPacket;


int main(int argc, char *argv[]){

	packetDrop = 0;
	packetsArr = 0;
	packetsQ2 = 0;
	packetList = (My402List*)malloc(sizeof(My402List));
	if (packetList == NULL){
		printf("Packet List could not be created\n");
		exit(1);
	}
	My402ListInit(packetList);
	mode = 0; 
	endVar = 0;
	lambda = 1;
	mu = 0.35;
	r = 1.5;
	p = 3;
	num = 20;
	B = 10;
	int tokenThread,packetThread,server1Thread,server2Thread,sigRes;

	sigemptyset(&set);
    sigaddset(&set,SIGINT);
    sigprocmask(SIG_BLOCK,&set,0);
	
	int arr_size = 1;
	while (arr_size<argc)
	{
		if ((strncmp("-t",argv[arr_size],strlen(argv[arr_size])) == 0)){
			if (arr_size == (argc -1)){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			mode = 1;
			tsfile = argv[arr_size+1]; 
		}
		else if (strncmp("-lambda",argv[arr_size],strlen(argv[arr_size]))== 0){
			if (arr_size == argc-1){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%lf", &lambda) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			lambda = atof(argv[arr_size+1]);
		}
		else if (strncmp("-mu",argv[arr_size],strlen(argv[arr_size])) == 0){
			if (arr_size == argc-1){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%lf", &mu) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			mu = atof(argv[arr_size+1]);
			printf("mu is : %f\n", mu);
		}
		else if (strncmp("-n",argv[arr_size],strlen(argv[arr_size])) == 0){
			if(arr_size == (argc-1)){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%d", &num) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			num = atoi(argv[arr_size+1]);
		}
		else if (strncmp("-r",argv[arr_size],strlen(argv[arr_size])) == 0){
			if (arr_size == (argc-1)){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%lf", &r) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			r = atof(argv[arr_size+1]);
		}
		else if (strncmp("-B",argv[arr_size],strlen(argv[arr_size])) == 0){
			if(arr_size == (argc-1)){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%d", &B) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			B = atoi(argv[arr_size+1]);
		}
		else if (strncmp("-P",argv[arr_size],strlen(argv[arr_size])) == 0){
			if (arr_size == (argc-1)){
				printf("Insufficient arguments\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			else if ((argv[arr_size+1][0] == '-') || (sscanf(argv[arr_size+1], "%d", &p) != 1) ){
				printf("Invalid data type in command line\n");
				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
				exit(1);
			}
			p = atoi(argv[arr_size+1]);
		}
		else {
			printf("Invalid argument in commandline\n");
			printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
			exit(1);
		}
		arr_size = arr_size +2;
	}

	if (mode == 0){
		if((1/r)>10){
//			printf("r exceeds\n");
			r = 0.1;
		}
		if ((1/lambda)>10){
			lambda = 0.1;
		}
		if ((1/mu)>10){
			mu = 0.1;
		}
	}
	myBucket = (Bucket *)malloc(sizeof(Bucket));
	Q1 = (My402List*)malloc(sizeof(My402List));
	Q2 = (My402List*)malloc(sizeof(My402List));
	myStats  = (Stats*)malloc(sizeof(Stats));
	My402ListInit(Q1);
	My402ListInit(Q2);
	myBucket->tokenCount = 0;
	myBucket->B = B;
	myBucket->tokenAccepted = 0;
	myBucket->tokenDropped = 0;
	myBucket->rate = (1/r)*1000;
	myStats->p_interarrival_sum =0; 
	myStats->avg_q1 = 0;
	myStats->avg_q2 = 0;
	myStats->avg_s1 = 0;
	myStats->avg_s2 = 0;
	myStats->totalTime = 0;
	myStats->totalTimeSq = 0;
	myStats->serviceSum = 0;
	printEmulationParameters();
	gettimeofday(&timeStart,NULL);
	printf("%012.3fms: emulation begins\n",timeDiff(&timeStart,&timeStart));
	tokenThread = pthread_create(&tokenDep,NULL,startToken,myBucket);
	packetThread = pthread_create(&packetArr,NULL, generatePacket,packetList);
	server1Thread = pthread_create(&server1,NULL,startServer1,Q2);
	server2Thread = pthread_create(&server1,NULL,startServer2,Q2);
	sigRes = pthread_create(&sigthread,NULL,monitor,0);
	if (tokenThread){
		printf("Ërror in Token Arrival thread creation\n");
		exit(1);
	}
	if (packetThread) {
		printf("Error in Packet Arrival thread creation\n");
		exit(1);
	}
	if (server1Thread) {
		printf("Error in Packet Arrival thread creation\n");
		exit(1);
	}
	if (server2Thread) {
		printf("Error in Packet Arrival thread creation\n");
		exit(1);
	}
	if (sigRes){
		printf("Error in signal capture thread creation\n");
		exit(1);
	}
	pthread_join(packetArr,0);
	pthread_join(tokenDep,0);	
	pthread_join(server1,0);
	pthread_join(server2,0);
	gettimeofday(&timeEnd,NULL);
	printf("Time elapsed: %012.3fms\n",(timeDiff(&timeStart,&timeEnd))/1000);
	calcStats();
	free(myBucket);
	free(myStats);
	return 1;

}


void *monitor(void *input){
	int siginput;
	My402ListElem* elemToRemove;
	Packet *packetToRemove;
	sigwait(&set,&siginput);
	pthread_cancel(tokenDep);
	pthread_cancel(packetArr);
	endVar = 1;
	pthread_mutex_lock(&m1);
	if (!My402ListEmpty(Q1)){
		while(!My402ListEmpty(Q1)){
			elemToRemove =My402ListFirst(Q1);
			packetToRemove = (Packet *)elemToRemove->obj;
			printf("p%d removed from Q1\n",packetToRemove->id);
			My402ListUnlink(Q1,elemToRemove);
		}
	}
	if (!My402ListEmpty(Q2)){
		while(!My402ListEmpty(Q2)){
			elemToRemove =My402ListFirst(Q2);
			packetToRemove = (Packet *)elemToRemove->obj;
			printf("p%d removed from Q2\n",packetToRemove->id);
			My402ListUnlink(Q1,elemToRemove);
		}
	}
	pthread_cond_broadcast(&cv);
	pthread_mutex_unlock(&m1);
	return NULL;

}

void calcStats()
{
	double ans = 0;
	//double totalEmulationTime = (double)((timeDiff(&timeStart,&timeEnd))/1000);
	printf("Statistics:\n");
	printf("\taverage packet inter-arrival time = %.6g\n",((myStats->p_interarrival_sum/(double)num))/1000);
	if (packetsArr!=0){
		printf("\taverage packet service time = %.6g\n",(((myStats->serviceSum)/(double)packetsArr)/1000)); 
	}
	else {
		printf("\taverage packet service time = N/A since no packets were served\n");
	}
	//printf("avg Q1 sum = %.6g\n",(myStats->avg_q1));
	//printf("total emulation time = %.6g\n",(timeDiff(&timeStart,&timeEnd))/1000);
    printf("\taverage number of packets in Q1 = %.6g\n",((myStats->avg_q1)/((double)((timeDiff(&timeStart,&timeEnd))/1000))));
   // printf("avg Q2 sum = %.6g\n",(myStats->avg_q2));
    printf("\taverage number of packets in Q2 = %.6g\n",((myStats->avg_q2)/((double)((timeDiff(&timeStart,&timeEnd))/1000))));    
    printf("\taverage number of packets at S1 = %.6g\n",((myStats->avg_s1)/((double)((timeDiff(&timeStart,&timeEnd))/1000))));
    printf("\taverage number of packets at S2 = %.6g\n",((myStats->avg_s2)/((double)((timeDiff(&timeStart,&timeEnd))/1000))));    
    if ((num-packetDrop)!=0){
    	double avg = (myStats->totalTime)/((double)(num-packetDrop));
   		double avgSq = ((myStats->totalTimeSq)/((double)(num-packetDrop)));
    	printf("\taverage time a packet spent in system = %.6g\n",avg/1000);
    	printf("\tstandard deviation for time spent in system = %.6g\n",(sqrt((avgSq-(avg*avg))))/1000);
    }
    else {
 		printf("\taverage time a packet spent in system = N/A since no packets were served in the system\n");
    	printf("\tstandard deviation for time spent in system = N/A since no packets were served in the system\n");	
    }
    double drops = (double)myBucket->tokenDropped;
    double totalTok = (double)myBucket->tokenAccepted+(double)myBucket->tokenDropped;
    if (totalTok!=0){
    	    printf("\ttoken drop probability = %.6g \n",(drops/totalTok));
    }
    else {
    	printf("\ttoken drop probability = %.6g (N/A since there are were no tokens) \n",ans);
    }
    if (num!=0){
    	printf("\tpacket drop probability = %.6g \n",((double)packetDrop/(double)num));
    }
    else {
    	printf("\tpacket drop probability = %.6g (N/A since there were no packets) \n",ans);
    }
	return;

}

void printEmulationParameters()
{
	 printf("Emulation Parameters:\n");
	 printf("\tnumber to arrive = %d\n",num);
	 if (mode==0){
	 	 printf("\tlambda = %0.2f\n",lambda);       //   (print this line only if -t is not specified)
	 	 printf("\tmu = %0.2f\n",mu);       //      (print this line only if -t is not specified)
	 }
     printf("\tr = %0.2f\n",r);
     printf("\tB = %d\n",B);
     if(mode == 0){
     	printf("\tP = %d\n",p);	         //  (print this line only if -t is not specified)
     }
     if (mode == 1){
     	printf("\ttsfile = %s\n",tsfile);  	  	//     (print this line only if -t is specified)   	
     }  
}


void *startToken(void *b)
{
	Bucket *bucket = (Bucket *)b;
	struct timeval tokenTime1, tokenTime2,tokenAdd;
	Packet *q1Packet, *q2packet1;
	My402ListElem *elem2,*elem1;
	double rate = bucket->rate; 
	gettimeofday(&tokenTime1,NULL);

	double timeToDisplay = (timeDiff(&timeStart,&tokenTime1))/1000;
	if (timeToDisplay<rate){
			double sleeptime = (rate - timeToDisplay)*1000;
			usleep(sleeptime);
			gettimeofday(&tokenTime1,NULL);
	}
	pthread_mutex_lock(&m1);
	bucket->tokenCount++; 
	bucket->tokenAccepted++;
	timeToDisplay = (timeDiff(&timeStart,&tokenTime1))/1000;
	printf("%012.3fms: token t%d arrives, token bucket now has %d token\n",timeToDisplay,((bucket->tokenAccepted + bucket->tokenDropped)),bucket->tokenCount);
	pthread_mutex_unlock(&m1);
	while (true)
	{
		gettimeofday(&tokenTime2,NULL);
		double tokenDiff = (timeDiff(&tokenTime1,&tokenTime2))/1000;
		if (tokenDiff<rate){
			double sleeptime = (rate - tokenDiff)*1000;
			usleep(sleeptime);
			gettimeofday(&tokenTime2,NULL);
		}
		pthread_mutex_lock(&m1);
		
		if ((packetsQ2 + packetDrop) >= num){
		
			pthread_mutex_unlock(&m1);
		
			break;
		}
		timeToDisplay = (timeDiff(&(timeStart),&tokenTime2))/1000;
		printf("%012.3fms: token t%d arrives, ",timeToDisplay,(bucket->tokenAccepted + bucket->tokenDropped));
		if (bucket->tokenCount < bucket->B)
		{
			bucket->tokenAccepted++;
			bucket->tokenCount++;
			printf("token bucket now has %d token\n",bucket->tokenCount);
		}
		else {
			printf("dropped\n");
			bucket->tokenDropped++;
		}
		elem1 = My402ListFirst(Q1);
		if (elem1 !=NULL){
			q1Packet = (Packet *)malloc(sizeof(Packet));
			elem2 = My402ListFirst(Q1);
			q2packet1 = (Packet *)elem2->obj;
			q1Packet->serviceTime = q2packet1->serviceTime;
			q1Packet->tokenCount = q2packet1->tokenCount;;
			q1Packet->lambdaRate = q2packet1->lambdaRate;
			q1Packet->id = q2packet1->id;
			q1Packet->arrivalTime = q2packet1->arrivalTime;
			q1Packet->q1enter = q2packet1->q1enter;
      		if ((myBucket->tokenCount >= q1Packet->tokenCount))
        	{
        		myBucket->tokenCount = myBucket->tokenCount - q1Packet->tokenCount;
        		gettimeofday(&tokenAdd,NULL);
        		printf("%012.3fms: p%d leaves Q1, time in Q1 = %07.3f, token bucket now has %d token\n",(timeDiff(&timeStart,&tokenAdd))/1000,q1Packet->id,(timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000,myBucket->tokenCount);
       			My402ListAppend(Q2,(void *)q1Packet);
       	 		gettimeofday(&(q1Packet->q2enter),NULL);
        		printf("%012.3fms: p%d enters Q2\n",(timeDiff(&timeStart,&(q1Packet->q2enter)))/1000,q1Packet->id);
        		My402ListUnlink(Q1,elem1);
        		pthread_cond_broadcast(&cv);
        		packetsQ2++;
        		myStats->avg_q1 += (timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000;
       	 	}
        }
        tokenTime1 = tokenTime2;
		pthread_mutex_unlock(&m1);

	}
	free(q1Packet);
	return NULL;
}
void *generatePacket()
{
	//My402List *myList = (My402List *)myL;
	struct timeval packetTime1, packetTime2, queueEnter,tokenAdd;
	Packet *myPacket;
	int lineCount = 0;
	buffer[1024] = '\0';
	if (mode == 1)
	{
		fp = fopen(tsfile,"r");
		if (fp == NULL){
			printf("File %s couldn't be opened\n",tsfile);
			fclose(fp);
	//		printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
			exit(1);
		}
			fgets(buffer,sizeof(buffer),fp);
			int i=0;
			while (buffer[i]!='\n')
			{
				if (isdigit(buffer[i]) == 0){
					printf("Invalid data type in the file - line 1 is not just a number\n");
					fclose(fp);
	//				printf("usage: warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
        			exit(1);
				}
				else if (buffer[i] == ' ' || buffer[i] == '\t' || buffer[i] == '\0' || buffer[i] == '\n'){
    				printf("Line has leading/trailing spaces\n");
    				fclose(fp);
    				exit(1);
    			}
    			i++;
			}
			char *ret = strchr(buffer, '.');
			if (ret!=NULL){
				printf("Invalid data type: '.' found\n");
				fclose(fp);
				exit(1);
			}
    		num = atoi(buffer);
			while (fgets(buffer,sizeof(buffer),fp) != NULL)
			{
				ret = strchr(buffer, '.');
				char *ret2 = strchr(buffer,'-');
				if (buffer[1024]!='\0'){
					printf("Buffer overflow: your line is too long\n");
					fclose(fp);
					exit(1);
				}
				else if (buffer[0] == ' ' || buffer[0] == '\t' || buffer[0] == '\0' || buffer[0] == '\n'){
    				printf("Line has leading/trailing spaces or tab character\n");
    				fclose(fp);
    				exit(1);
    			}
				else if (ret!=NULL || ret2!=NULL){
					printf("Invalid data type: '.' or '-' found\n");
					fclose(fp);
					exit(1);
				}
				int j=0,k=0,l=0;
				char field1[200],field2[200],field3[200];
				while (buffer[j]!=' ' && buffer[j]!='\t'){
					if (!isdigit(buffer[j])){
						printf("Invalid data type in the file\n");
						fclose(fp);
        				exit(1);
					}
					field1[j] = buffer[j];
					j++;
				}
				field1[j]='\0';
				int interArrival = atoi(field1);
				while(buffer[j]== ' ' || buffer[j]=='\t'){ 
					j++;	
				}
				if (buffer[j]=='\n'){printf("Insufficient arguments: part1\n"); exit(1);}
				while (buffer[j]!=' ' && buffer[j]!='\t'){
					if (!isdigit(buffer[j])){
						printf("Invalid data type in the file\n");
						fclose(fp);
        				exit(1);
					}
					field2[k] = buffer[j];
					j++;
					k++;
				}
				field2[k]='\0';
				int tokens = atoi(field2);
				while(buffer[j]== ' ' || buffer[j]=='\t'){ 
					j++;	
				}
				if (buffer[j]=='\n'){printf("Insufficient arguments: part2\n"); exit(1);}
				while (buffer[j]!=' ' && buffer[j]!='\t' && buffer[j]!='\n')
				{
					if (!isdigit(buffer[j])){
						printf("Invalid data type in the file\n");
						fclose(fp);
        				exit(1);
					}
					field3[l] = buffer[j];
					j++;
					l++;
				}
				field3[l]='\0';
				int service = atoi(field3);
				if (buffer[j]!='\n'){printf("Trailing spaces: part3\n"); exit(1);}
				pthread_cleanup_push(free,myPacket);
     			myPacket = (Packet *)malloc(sizeof(Packet));
       			pthread_cleanup_pop(0);
				myPacket->serviceTime = service;
				myPacket->tokenCount = tokens;
				myPacket->lambdaRate = interArrival;
				myPacket->id = lineCount+1;
				lineCount++;
				gettimeofday(&packetTime1,NULL);
				gettimeofday(&packetTime2,NULL);
				double arrivalP2 = (timeDiff(&packetTime1,&packetTime2))/1000;
				if (arrivalP2<(myPacket->lambdaRate)){
					double sleeptime = ((myPacket->lambdaRate) - arrivalP2)*1000;  
					usleep(sleeptime);
					gettimeofday(&packetTime2,NULL);
					arrivalP2 = (timeDiff(&packetTime1,&packetTime2))/1000;
				}
				double timeToDisplay = (timeDiff(&timeStart,&packetTime2))/1000;
				myPacket->arrivalTime = packetTime2;
				pthread_mutex_lock(&m1);
				if((packetsQ2+packetDrop) >= num )
	     		{
	     			free(Q1);
    	 			pthread_mutex_unlock(&m1);
    	 			break;
    	 		}
    	 		if ((myPacket->tokenCount) > (myBucket->B)){
					printf("%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %06.3f, dropped\n",timeToDisplay,myPacket->id,myPacket->tokenCount,arrivalP2);
					myStats->p_interarrival_sum = myStats->p_interarrival_sum + arrivalP2;
					packetDrop++;
				}
				else {
					printf("%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %06.3f\n",timeToDisplay,myPacket->id,myPacket->tokenCount,arrivalP2);
					myStats->p_interarrival_sum = myStats->p_interarrival_sum + arrivalP2;			
					Packet *newPacket = (Packet *)malloc(sizeof(Packet));
					newPacket->serviceTime = myPacket->serviceTime;
					newPacket->tokenCount = myPacket->tokenCount;;
					newPacket->lambdaRate = myPacket->lambdaRate;
					newPacket->id = myPacket->id;
					newPacket->arrivalTime = myPacket->arrivalTime;
					My402ListAppend(Q1,(void *)newPacket);
					gettimeofday(&queueEnter,NULL);
					myPacket->q1enter = queueEnter;
					newPacket->q1enter = queueEnter;
					timeToDisplay = (timeDiff(&timeStart,&queueEnter))/1000;
					printf("%012.3fms: p%d enters Q1\n",timeToDisplay,newPacket->id);
				}
				My402ListElem *elem1 = My402ListFirst(Q1);
				if (elem1 !=NULL){
					Packet *q1Packet = (Packet *)malloc(sizeof(Packet));
					My402ListElem *elem2 = My402ListFirst(Q1);
					Packet *q2packet1 = (Packet *)elem2->obj;
					q1Packet->serviceTime = q2packet1->serviceTime;
					q1Packet->tokenCount = q2packet1->tokenCount;;
					q1Packet->lambdaRate = q2packet1->lambdaRate;
					q1Packet->id = q2packet1->id;
					q1Packet->arrivalTime = q2packet1->arrivalTime;
					q1Packet->q1enter = q2packet1->q1enter;
      				if ((myBucket->tokenCount >= q1Packet->tokenCount))
        			{
        				myBucket->tokenCount = myBucket->tokenCount - q1Packet->tokenCount;
        				gettimeofday(&tokenAdd,NULL);
        				printf("%012.3fms: p%d leaves Q1, time in Q1 = %07.3f, token bucket now has %d token\n",(timeDiff(&timeStart,&tokenAdd))/1000,q1Packet->id,(timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000,myBucket->tokenCount);
       					My402ListAppend(Q2,(void *)q1Packet);
       	 				gettimeofday(&(q1Packet->q2enter),NULL);
        				printf("%012.3fms: p%d enters Q2\n",(timeDiff(&timeStart,&(q1Packet->q2enter)))/1000,q1Packet->id);
        				pthread_cond_broadcast(&cv);
        				My402ListUnlink(Q1,elem1);
        				packetsQ2++;
        				myStats->avg_q1 += (timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000;
       	 			}
        		}
				if (lineCount==num){
					pthread_mutex_unlock(&m1);			
					break;
				}
        		packetTime1 = packetTime2;
				pthread_mutex_unlock(&m1);
			}
		}

	else {
		int count=0;
		gettimeofday(&packetTime1,NULL);
		while (count<num)
		{
			pthread_cleanup_push(free,myPacket);
			myPacket = (Packet *)malloc(sizeof(Packet));
			pthread_cleanup_pop(0);
			myPacket->serviceTime = (1/mu)*1000;
			myPacket->tokenCount = p;
			myPacket->lambdaRate = (1/lambda)*1000;
			myPacket->id = count+1;
			gettimeofday(&packetTime2,NULL);
			double arrivalP2 = (timeDiff(&packetTime1,&packetTime2))/1000;
			if (arrivalP2<(myPacket->lambdaRate)){
				double sleeptime = ((myPacket->lambdaRate) - arrivalP2)*1000;  
				usleep(sleeptime);
				gettimeofday(&packetTime2,NULL);
				arrivalP2 = (timeDiff(&packetTime1,&packetTime2))/1000;
			}
			double timeToDisplay = (timeDiff(&timeStart,&packetTime2))/1000;
			myPacket->arrivalTime = packetTime2;
			pthread_mutex_lock(&m1);
			if((packetsQ2+packetDrop) >= num )
	     	{
	     		free(Q1);
    	 		pthread_mutex_unlock(&m1);
    	 		break;
    	 	}

			if (myPacket->tokenCount > myBucket->B){
				printf("%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %06.3f, dropped\n",timeToDisplay,myPacket->id,myPacket->tokenCount,arrivalP2);
				myStats->p_interarrival_sum = myStats->p_interarrival_sum + arrivalP2;
				packetDrop++;
			}
			else {
				printf("%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %06.3f\n",timeToDisplay,myPacket->id,myPacket->tokenCount,arrivalP2);
				myStats->p_interarrival_sum = myStats->p_interarrival_sum + arrivalP2;			
				Packet *newPacket = (Packet *)malloc(sizeof(Packet));
				newPacket->serviceTime = myPacket->serviceTime;
				newPacket->tokenCount = myPacket->tokenCount;;
				newPacket->lambdaRate = myPacket->lambdaRate;
				newPacket->id = myPacket->id;
				newPacket->arrivalTime = myPacket->arrivalTime;
				My402ListAppend(Q1,(void *)newPacket);
				gettimeofday(&queueEnter,NULL);
				myPacket->q1enter = queueEnter;
				newPacket->q1enter = queueEnter;
				timeToDisplay = (timeDiff(&timeStart,&queueEnter))/1000;
				//printf("%012.3fms: p%d enters Q1\n",timeToDisplay,newPacket->id);
			}
			My402ListElem *elem1 = My402ListFirst(Q1);
			if (elem1 !=NULL){
				Packet *q1Packet = (Packet *)malloc(sizeof(Packet));
				My402ListElem *elem2 = My402ListFirst(Q1);
				Packet *q2packet1 = (Packet *)elem2->obj;
				q1Packet->serviceTime = q2packet1->serviceTime;
				q1Packet->tokenCount = q2packet1->tokenCount;;
				q1Packet->lambdaRate = q2packet1->lambdaRate;
				q1Packet->id = q2packet1->id;
				q1Packet->arrivalTime = q2packet1->arrivalTime;
				q1Packet->q1enter = q2packet1->q1enter;
      			if ((myBucket->tokenCount >= q1Packet->tokenCount))
        		{
        			myBucket->tokenCount = myBucket->tokenCount - q1Packet->tokenCount;
        			gettimeofday(&tokenAdd,NULL);
        			printf("%012.3fms: p%d leaves Q1, time in Q1 = %07.3f, token bucket now has %d token\n",(timeDiff(&timeStart,&tokenAdd))/1000,q1Packet->id,(timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000,myBucket->tokenCount);
       				My402ListAppend(Q2,(void *)q1Packet);
       	 			gettimeofday(&(q1Packet->q2enter),NULL);
        			printf("%012.3fms: p%d enters Q2\n",(timeDiff(&timeStart,&(q1Packet->q2enter)))/1000,q1Packet->id);
        			pthread_cond_broadcast(&cv);
        			My402ListUnlink(Q1,elem1);
        			packetsQ2++;
        			myStats->avg_q1 += (timeDiff(&(q1Packet->q1enter),&tokenAdd))/1000;
       	 		}
        	}
        	//elem = My402ListNext(myList,elem);
        	count++;
			if (count == num){
				pthread_mutex_unlock(&m1);			
				break;
			}
			//packet1 = (Packet *)(elem->obj);
        	packetTime1 = packetTime2;
			pthread_mutex_unlock(&m1);
	//---------------------------------------------------------------------------------------------------------------------------------------------------//
		
		}
	}
	
	if (mode == 1)
	{
		if (lineCount!=num){
			printf("Packet count insufficient:%d\n",lineCount);
			fclose(fp);
			exit(1);
		}
		fclose(fp);
	}
	pthread_mutex_lock(&m1);
    pthread_cond_broadcast(&cv);
    pthread_mutex_unlock(&m1);
	return NULL;
}

double timeDiff(struct timeval *start,struct timeval *end)
{
	double endInMicro = (double)end->tv_sec*1000000 + (double)end->tv_usec;
	double startInMicro = (double) start->tv_sec*1000000 + (double)start->tv_usec; 
	double elapsed = (double)endInMicro - (double)startInMicro;
	return elapsed;
}


void *startServer1(){
	struct timeval leaveQ2,startService,endService;
	Packet *q2Packet;//= (Packet *)malloc(sizeof(Packet));
	My402ListElem *q2Elem; //= (My402ListElem *)malloc(sizeof(My402ListElem));
	while(1){


		pthread_mutex_lock(&m1);
		if (endVar == 1 || (((packetsArr + packetDrop == num) && (packetsArr == packetsQ2)))){
			pthread_mutex_unlock(&m1);
			break;
		}
		while(My402ListEmpty(Q2) && endVar == 0){
			if (endVar == 1 || (((packetsArr + packetDrop == num) && (packetsArr == packetsQ2)))){
				pthread_mutex_unlock(&m1);
				break;
			}
			pthread_cond_wait(&cv, &m1);
		}
		q2Elem = My402ListFirst(Q2);
	if (q2Elem){


		q2Packet = (Packet *)q2Elem->obj;
		My402ListUnlink(Q2,q2Elem);
		gettimeofday(&leaveQ2,NULL);
		double q2Diff = (timeDiff(&timeStart,&leaveQ2))/1000;
		double diff = (timeDiff(&(q2Packet->q2enter),&leaveQ2))/1000;
		printf("%012.3fms: p%d leaves Q2, time in Q2 = %07.3fms\n",q2Diff,q2Packet->id,diff);
		myStats->avg_q2 = myStats->avg_q2 + diff;
		//printf("unlocking in S1 thread p1\n");
		pthread_mutex_unlock(&m1);
		
		gettimeofday(&startService,NULL);
		q2Diff = (timeDiff(&(timeStart),&startService))/1000;
		printf("%012.3fms: p%d begins service at S1, requesting %07.3fms of service\n",q2Diff,q2Packet->id,q2Packet->serviceTime);
		double muVal = q2Packet->serviceTime;
		usleep(muVal*1000);
		gettimeofday(&endService,NULL);
		double totalTime = (timeDiff(&(q2Packet->arrivalTime),&endService))/1000;
		printf("%012.3fms: p%d departs from S1, service time = %07.3fms, time in system = %07.3fms\n",(timeDiff(&timeStart,&endService))/1000,q2Packet->id,(timeDiff(&startService,&endService))/1000,totalTime);
		

		pthread_mutex_lock(&m1);
		packetsArr++;
		myStats->totalTime += totalTime;
		//printf("myStats->totalTime:%f\n",myStats->totalTime);
		myStats->avg_s1 = myStats->avg_s1 + ((timeDiff(&startService,&endService))/1000);
		myStats->serviceSum = myStats->serviceSum + ((timeDiff(&startService,&endService))/1000);
		myStats->totalTimeSq += totalTime*totalTime;
		pthread_mutex_unlock(&m1);
	}
	
	}
	pthread_mutex_lock(&m1);
    pthread_cond_broadcast(&cv);
    pthread_mutex_unlock(&m1);
	return NULL;

}


void *startServer2(){
	struct timeval leaveQ2,startService,endService;
	Packet *q2Packet;//= (Packet *)malloc(sizeof(Packet));
	My402ListElem *q2Elem; //= (My402ListElem *)malloc(sizeof(My402ListElem));
	while(1)
	{
	
		pthread_mutex_lock(&m1);
		if (endVar == 1 || (((packetsArr + packetDrop == num) && (packetsArr == packetsQ2)))){
			free(Q2);
			pthread_mutex_unlock(&m1);
			break;
		}
		while(My402ListEmpty(Q2)){
		if (endVar == 1 || (((packetsArr + packetDrop == num) && (packetsArr == packetsQ2)))){
				pthread_mutex_unlock(&m1);
				break;
			}
			pthread_cond_wait(&cv, &m1);
		}
		q2Elem = My402ListFirst(Q2);
		if (q2Elem){

		q2Packet = (Packet *)q2Elem->obj;
		My402ListUnlink(Q2,q2Elem);
		gettimeofday(&leaveQ2,NULL);
		double q2Diff = (timeDiff(&timeStart,&leaveQ2))/1000;
		double diff = (timeDiff(&(q2Packet->q2enter),&leaveQ2))/1000;
		printf("%012.3fms: p%d leaves Q2, time in Q2 = %07.3fms\n",q2Diff,q2Packet->id,diff);
		myStats->avg_q2 = myStats->avg_q2 + diff;
		pthread_mutex_unlock(&m1);

		gettimeofday(&startService,NULL);
		q2Diff = (timeDiff(&(timeStart),&startService))/1000;
		printf("%012.3fms: p%d begins service at S2, requesting %07.3fms of service\n",q2Diff,q2Packet->id,q2Packet->serviceTime);
		double muVal = q2Packet->serviceTime;
		usleep(muVal*1000);
		gettimeofday(&endService,NULL);
		double totalTime = (timeDiff(&(q2Packet->arrivalTime),&endService))/1000;
		printf("%012.3fms: p%d departs from S2, service time = %07.3fms, time in system = %07.3fms\n",(timeDiff(&timeStart,&endService))/1000,q2Packet->id,(timeDiff(&startService,&endService))/1000,totalTime);
		
		pthread_mutex_lock(&m1);
		packetsArr++;
		myStats->serviceSum = myStats->serviceSum + ((timeDiff(&startService,&endService))/1000);
		myStats->avg_s2 = myStats->avg_s2 + ((timeDiff(&startService,&endService))/1000);
		myStats->totalTime += totalTime;
		//printf("myStats->totalTime:%f\n",myStats->totalTime);
		myStats->totalTimeSq += totalTime*totalTime;
		pthread_mutex_unlock(&m1);
	}
	
	}
	pthread_mutex_lock(&m1);
    pthread_cond_broadcast(&cv);
    pthread_mutex_unlock(&m1);
	return NULL;

}





